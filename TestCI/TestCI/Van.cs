﻿namespace TestCI
{
    public class Van : Vehicle
    {
        private int no = 2;

        public override string run()
        {
            return "Van Running";
        }

        public string run(string test)
        {
            return "Van Running " + test;
        }

        public override string stop()
        {
            return "Van Stopped";
        }

        public override int improve(int add)
        {
            no += add;
            return no;
        }
    }
}
