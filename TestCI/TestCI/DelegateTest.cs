﻿using System;
using System.Windows.Forms;

namespace TestCI
{
    class DelegateTest
    {

        private string[] friends = {"David","Ryan","Adam","Briony","Harry","Sara",
        "Peter","Maria","Natacha","Lydia","Nina","Macha",
        "Maggy","Victoria","Naomi"};

        public void mess(string str, ListBox lb)
        {

            lb.Items.Clear();  //ล้าง ListBox
            int i = 0;
            while (i < 500)
            {
                //สุ่มชื่อเพื่อนขึ้นมา
                string fName = friends[new Random().Next(0, friends.Length)];
                lb.Items.Add(str + " " + fName);
                lb.SelectedIndex = i;
                i++;
            }
        }
    }
}
