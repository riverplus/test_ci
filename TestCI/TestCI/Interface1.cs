﻿namespace TestCI
{
    interface Interface1
    {
        string run();
    }

    class runSpeed : Interface1
    {
        public string run()
        {
            return "Run Speed";
        }
    }

    class runNormal : Interface1
    {
        public string run()
        {
            return "Run Normal";
        }
    }
}
