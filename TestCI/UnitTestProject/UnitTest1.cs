﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestCI;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest
    {
        Vehicle vehTest1 = new Car();
        Vehicle vehTest2 = new Van();

        [TestMethod]
        public void TestMethodRunCar()
        {
            vehTest1.run();
        }

        [TestMethod]
        public void TestMethodRunVan()
        {
            vehTest2.run();
        }

        [TestMethod]
        public void TestMethodStopCar()
        {
            vehTest1.stop();
        }
    
        [TestMethod]
        public void TestMethodStopVan()
        {
            vehTest2.stop();
        }

        [TestMethod]
        public void TestMethodImproveCar()
        {
            int result1 = vehTest1.improve(10);
            Assert.AreEqual(15, result1);
        }

        [TestMethod]
        public void TestMethodImproveVan()
        {
            int result2 = vehTest2.improve(10);
            Assert.AreEqual(12, result2);
        }

    }
}
