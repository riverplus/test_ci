﻿namespace TestCI
{
    class ClassAbstraction
    {
        Interface1 inf1;
        Interface2 inf2;

        public virtual string performRun()
        {
            return inf1.run();
        }

        public virtual string performWalk()
        {
            return inf2.walk();
        }

        public virtual void setRun(Interface1 infR)
        {
            inf1 = infR;
        }

        public virtual void setWalk(Interface2 infW)
        {
            inf2 = infW;
        }

    }

}
