﻿namespace TestCI
{
    public class Vehicle
    {
        private int no = 0;

        public int TestNo { get; set; }

        public virtual string run()
        {
            return "Running";
        }

        public virtual string stop()
        {
            return "Stopped";
        }
        public virtual int improve(int add)
        {
            no += add;
            return no;
        }

    }
}
