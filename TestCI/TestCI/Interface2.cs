﻿namespace TestCI
{
    interface Interface2
    {
        string walk();
    }

    class walkSpeed : Interface2
    {
        public string walk()
        {
            return "Walk Speed";
        }
    }

    class walkNormal : Interface2
    {
        public string walk()
        {
            return "Walk Normal";
        }
    }
}
