﻿namespace TestCI
{
    public class Car : Vehicle
    {
        private int no = 5;
        
        public override string run()
        {
            return "Car Running";
        }

        public override string stop()
        {
            return "Car Stopped";
        }

        public override int improve(int add)
        {
            no += add;
            return no;
        }
    }
}
