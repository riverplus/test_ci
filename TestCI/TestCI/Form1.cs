﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace TestCI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Vehicle vehicle1 = new Car();
        Vehicle vehicle2 = new Van();
        Vehicle vehicle = new Vehicle();
        Van van = new Van();

        private void Form1_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();         
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(vehicle1.run());
            MessageBox.Show(vehicle.run());
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show(vehicle1.stop());
            MessageBox.Show(vehicle.stop());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(vehicle2.run());
            MessageBox.Show(van.run("test"));
            MessageBox.Show(vehicle.run());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show(vehicle2.stop());
            MessageBox.Show(vehicle.stop());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string data = vehicle1.improve(Convert.ToInt32(textBox1.Text)).ToString();
            MessageBox.Show(data);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string data = vehicle2.improve(Convert.ToInt32(textBox1.Text)).ToString();
            MessageBox.Show(data);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            // Create new entities from Entities
            using (var db = new myDatabaseEntities())
            {
                // Insert
                db.QC.Add(new QC()
                {
                    QCTYPE_ID = 1,
                    SELECTED = 10,
                    TIME = 10,
                    MINUTE = 10,
                    SEQUENCE = 1,
                });

                db.SaveChanges();

                // Get data 
                var ds = db.QC.ToList();
                // Assign to DataGridView
                this.dataGridView1.DataSource = ds;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            // Create new entities from Entities
            using (var db = new myDatabaseEntities())
            {
                // Update
                int strID = 5;

                // Update Statement
                var update = db.QC.Where(o => o.ID == strID).FirstOrDefault();
                if (update != null)
                {
                    update.SELECTED = Convert.ToInt32(textBox1.Text);
                    update.TIME = Convert.ToInt32(textBox1.Text);
                    update.MINUTE = Convert.ToInt32(textBox1.Text);
                    update.SEQUENCE = 1;
                }

                db.SaveChanges();

                // Get data 
                var ds = db.QC.ToList();
                // Assign to DataGridView
                this.dataGridView1.DataSource = ds;
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            // Create new entities from Entities
            using (var db = new myDatabaseEntities())
            {
                // Get data 
                var ds = db.QC.ToList();
                // Assign to DataGridView
                this.dataGridView1.DataSource = ds;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            ClassAbstraction clsab = new ClassAbstraction();

            clsab.setRun(new runNormal());
            clsab.setWalk(new walkNormal());

            //clsab.setRun(new runSpeed());
            //clsab.setWalk(new walkSpeed());

            string run =  clsab.performRun();
            string walk = clsab.performWalk();

            MessageBox.Show(run + Environment.NewLine + walk);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            ClassAbstraction clsab = new ClassAbstraction();

            //clsab.setRun(new runNormal());
            //clsab.setWalk(new walkNormal());

            clsab.setRun(new runSpeed());
            clsab.setWalk(new walkSpeed());

            string run = clsab.performRun();
            string walk = clsab.performWalk();

            MessageBox.Show(run + Environment.NewLine + walk);
        }

        public delegate void showMS(string txt, ListBox l);

        private void button13_Click(object sender, EventArgs e)
        {
            DelegateTest delTest = new DelegateTest();
            //ปิดการตรวจสอบ Thread Cross
            Form1.CheckForIllegalCrossThreadCalls = false;

            //สร้าง delegate method
            showMS sms = new showMS(delTest.mess);

            ////ประกาศ   IAsyncResult  เพื่อรับค่าสถานะจาก delegate ที่มีการ invoke
            IAsyncResult helloRes = sms.BeginInvoke("Hello", this.listBox1, null, null);
            IAsyncResult byeRes = sms.BeginInvoke("Bye", this.listBox2, null, null);

        }

        private void button14_Click(object sender, EventArgs e)
        {
            vehicle1.TestNo = Convert.ToInt32(textBox2.Text);
            vehicle2.TestNo = Convert.ToInt32(textBox2.Text);

            MessageBox.Show("Car : " + vehicle1.TestNo.ToString() + Environment.NewLine +" Van : " + vehicle2.TestNo.ToString());
        }
    }
}
